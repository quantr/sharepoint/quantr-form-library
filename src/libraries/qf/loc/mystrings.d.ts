declare interface IQfLibraryStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'QfLibraryStrings' {
  const strings: IQfLibraryStrings;
  export = strings;
}
